---
title: "About"
description: "Database engineer by trade. Sometime I go beyond databases too."
featured_image: ''
---
<!--- {{< figure src="/images/monumental_valley.png" title="A drop in the ocean" >}} --> 

Started as an Oracle DBA. Worked on High-Availability, Performance tuning and reliability.

Currently working on AWS, Google Cloud & some other interesting projects.

Alumni of University of Arkansas (Walton College of Business) & SASTRA university